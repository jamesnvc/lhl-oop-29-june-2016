//
//  Calculator.h
//  OOPDemo
//
//  Created by James Cash on 29-06-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Calculator : NSObject

@property (nonatomic,assign) NSInteger magicNumber;

- (instancetype)initWithMagicNumber:(NSInteger)magic;

- (NSInteger)addToNumber:(NSInteger)x;

@end
