//
//  Calculator.m
//  OOPDemo
//
//  Created by James Cash on 29-06-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import "Calculator.h"

@implementation Calculator

- (instancetype)init
{
    self = [super init];
    if (self) {
        _magicNumber = 42;
    }
    return self;
}

- (instancetype)initWithMagicNumber:(NSInteger)magic
{
    self = [super init];
    if (self) {
        _magicNumber = magic;
    }
    return self;
}

- (NSInteger)addToNumber:(NSInteger)x
{
    return x + self.magicNumber;
}

@end
