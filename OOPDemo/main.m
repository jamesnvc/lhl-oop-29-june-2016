//
//  main.m
//  OOPDemo
//
//  Created by James Cash on 29-06-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Calculator.h"
#import "AdSupportedCalculator.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Calculator *calc1 = [[Calculator alloc] initWithMagicNumber:50];
        NSInteger value = 1;
        NSLog(@"%ld => %ld", value, [calc1 addToNumber:value]);

        AdSupportedCalculator *calc2 = [[AdSupportedCalculator alloc] initWithMagicNumber:2];
        NSLog(@"%ld => %ld", value, [calc2 addToNumber:value]);

        AdSupportedCalculator *calc3 = [[AdSupportedCalculator alloc] initWithMagicNumber:5 andSponsor:@"Lighthouse Labs"];
        NSLog(@"%ld => %ld", value, [calc3 addToNumber:value]);
    }
    return 0;
}
