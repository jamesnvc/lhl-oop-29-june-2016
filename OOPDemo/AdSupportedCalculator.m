//
//  AdSupportedCalculator.m
//  OOPDemo
//
//  Created by James Cash on 29-06-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import "AdSupportedCalculator.h"

@interface AdSupportedCalculator ()

@property (nonatomic,strong) NSString *sponsor;

@end

@implementation AdSupportedCalculator

- (instancetype)initWithMagicNumber:(NSInteger)magic
{
    self = [super initWithMagicNumber:magic];
    if (self) {
        NSLog(@"Magic calculator from our sponsor");
    }
    return self;
}

- (instancetype)initWithMagicNumber:(NSInteger)magic andSponsor:(NSString *)sponsor
{
    self = [super initWithMagicNumber:magic];
    if (self) {
        _sponsor = sponsor;
    }
    return self;
}

- (NSInteger)addToNumber:(NSInteger)x
{
    if (self.sponsor) {
        NSLog(@"This addition brought to you by %@", self.sponsor);
    } else {
        NSLog(@"Your ad here");
    }
    return 100 + [super addToNumber:x*2];
}

@end
