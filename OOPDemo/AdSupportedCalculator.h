//
//  AdSupportedCalculator.h
//  OOPDemo
//
//  Created by James Cash on 29-06-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import "Calculator.h"

@interface AdSupportedCalculator : Calculator

- (instancetype)initWithMagicNumber:(NSInteger)magic andSponsor:(NSString*)sponsor;

@end
